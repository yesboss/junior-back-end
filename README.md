# Junior Back End Engineer Test

## How to answer

-   [Fork this repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html), make it public
-   Install using npm: `npm install`
-   Read interfaces of objects and methods you have to implement in folder `./interfaces`
-   Read test cases in folder `./test`
-   Implement your unit tests
-   Implement your code in folder `./components`
-   Run your test: ```npm test```

## Tips
-   Commit often, push often
-   You may want to run `npm test` every now and then
-   Write your assumptions `ASSUMPTIONS.md` if you have any
